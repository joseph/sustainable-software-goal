# KDE Goal: Sustainable Software

**Sustainable Software** is one of the three [goals](https://kde.org/goals) selected by KDE in 2022 as a focus for the next two years. This goal is about creating, delivering and promoting software which preservers environment and society for us and future generations.

**Goal**: Promote Sustainable Software in KDE by

(i) aligning existing activities,

(ii) highlighting where our software is already sustainably designed

(iii) stimulating actions to increase sustainability, and

(iv) creating standards/tools to quantify software sustainability.

## This repository

Here we collect material and track progress on the [GitLab Board](https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards). Feel free to comment on or create issues or merge request, if you have any questions, ideas, or would like to contribute something, connect with us at the [KDE Eco](https://webchat.kde.org/#/room/#kde-eco:kde.org) Matrix room.

## More resources

* [KDE Goals landing page](https://kde.org/goals/)
* [KDE Goals Wiki](https://community.kde.org/Goals)
* [Original goal description](https://phabricator.kde.org/T15676)
* [KDE Eco](https://eco.kde.org)
* [Awesome sustainable resource](/Resources/awesome-sustainable-software.md)

## Contact

We are happy to hear from you and you are very welcome to join the goal effort for Sustainable Software. We get together in the KDE Eco community. There is a mailing list, a Matrix channel, and more. See https://eco.kde.org/get-involved/ for more information.

Feel also free to contact Cornelius Schumacher [\<schumacher@kde.org\>](mailto:schumacher@kde.org), the champion for the Sustainable Software goal, if you have any questions or ideas.

## License

The content in this repository is licensed under CC-BY-4.0, if not otherwise noted.
